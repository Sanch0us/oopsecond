package ru.nsu.fit.chuprakov.template.templateProcessor;

import java.util.Map;

public interface TemplateProcessor {
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException;
};
