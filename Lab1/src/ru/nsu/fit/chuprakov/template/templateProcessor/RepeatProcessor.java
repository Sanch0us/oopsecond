package ru.nsu.fit.chuprakov.template.templateProcessor;

import java.util.Map;

public class RepeatProcessor implements TemplateProcessor {
    private String iteration;
    private String value;

    public RepeatProcessor(String it, String val) {
        iteration = it;
        value = val;
    }

    ;

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (iterations.get(iteration) == null) {
            throw new ArgumentNotFoundException();
        }
        for (int count = iterations.get(iteration); count > 0; count--) {
            sb.append(value);
            sb.append('\n');
        }
    }
};