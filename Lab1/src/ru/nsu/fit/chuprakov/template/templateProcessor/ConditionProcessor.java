package ru.nsu.fit.chuprakov.template.templateProcessor;

import java.util.Map;

public class ConditionProcessor implements TemplateProcessor {
    private String condition;
    private String value;

    public ConditionProcessor(String cond, String val) {
        condition = cond;
        value = val;
    }

    ;

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (conditions.get(condition) == null) {
            throw new ArgumentNotFoundException();
        }
        sb.append(value);
    }
};