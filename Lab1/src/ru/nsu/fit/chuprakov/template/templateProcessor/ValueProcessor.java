package ru.nsu.fit.chuprakov.template.templateProcessor;

import java.util.Map;

public class ValueProcessor implements TemplateProcessor {
    private String value;

    public ValueProcessor(String str) {
        value = str;
    }

    ;

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (values.get(value) == null) {
            throw new ArgumentNotFoundException();
        }
        sb.append(values.get(value));
    }
};