package ru.nsu.fit.chuprakov.template.templateProcessor;

import java.util.Map;

public class StringProcessor implements TemplateProcessor {
    private String value;

    public StringProcessor(String str) {
        value = str;
    }

    ;

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        sb.append(value);
    }
};