package ru.nsu.fit.chuprakov.template;

import ru.nsu.fit.chuprakov.template.templateProcessor.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Template {
    private List<TemplateProcessor> processors;

    private int func(StringBuilder builder, String[] arr, int i) {
        while ((!arr[i].isEmpty()) && (i < arr.length) && (arr[i].charAt(arr[i].length() - 1) == '\\')) {
            builder.deleteCharAt(builder.length() - 1);
            builder.append('%');
            builder.append(arr[i + 1]);
            i++;
        }
        return i;
    }

    ;

    private int parseComplexConstruction(String[] splits, String str, int i) throws UnexpectedConstruction {
        String[] array = str.split(" ", 2);
        if ((array.length != 2) || (array[1].isEmpty()) || ((i + 1) >= splits.length)) {
            throw new UnexpectedConstruction();
        }
        StringBuilder bs1 = new StringBuilder(array[1]);
        StringBuilder bs2 = new StringBuilder(splits[i]);
        i = func(bs2, splits, i);
        bs1.deleteCharAt(bs1.length() - 1);
        switch (array[0]) {
            case "!if":
                processors.add(new ConditionProcessor(bs1.toString(), bs2.toString()));
                if (!splits[i + 1].equals("!endif!")) {
                    throw new UnexpectedConstruction();
                }
                break;
            case "!repeat":
                processors.add(new RepeatProcessor(bs1.toString(), bs2.toString()));
                if (!splits[i + 1].equals("!endrepeat!")) {
                    throw new UnexpectedConstruction();
                }
                break;
            default:
                throw new UnexpectedConstruction();
        }
        return i + 1;
    }

    ;

    private void parse(String[] splits) throws UnexpectedConstruction {
        boolean isTemp = false;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < splits.length; i++) {
            builder.setLength(0);
            builder.append(splits[i]);
            i = func(builder, splits, i);
            if (!isTemp) {
                processors.add(new StringProcessor(builder.toString()));
                isTemp = true;
                continue;
            }
            if ((builder.length() == 0) || (builder.charAt(0) != '!')) {
                processors.add(new ValueProcessor(builder.toString()));
                isTemp = false;
                continue;
            }
            i++;
            i = parseComplexConstruction(splits, builder.toString(), i);
            isTemp = false;
        }
    }

    ;

    public Template(String s) throws UnexpectedConstruction {
        processors = new ArrayList<TemplateProcessor>();
        String[] arr1 = s.split("%", -1);
        parse(arr1);
    }

    public String fill(Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        StringBuilder builder = new StringBuilder();
        for (TemplateProcessor processor : processors) {
            processor.fillTemplate(builder, values, conditions, iterations);
        }
        return builder.toString();
    }
}
