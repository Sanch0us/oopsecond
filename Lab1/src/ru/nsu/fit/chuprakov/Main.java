package ru.nsu.fit.chuprakov;

import ru.nsu.fit.chuprakov.template.templateProcessor.ArgumentNotFoundException;
import ru.nsu.fit.chuprakov.template.Template;
import ru.nsu.fit.chuprakov.template.UnexpectedConstruction;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        StringBuilder bs = new StringBuilder();
        try (FileReader fr = new FileReader("C:\\template.txt");) {
            Scanner scan = new Scanner(fr);
            while (scan.hasNextLine()) {
                bs.append(scan.nextLine()).append('\n');
            }
        } catch (Exception E) {
            return;
        }
        Map<String, String> values = new HashMap<>();
        values.put("name", "Vasya");
        Map<String, Boolean> conditions = new HashMap<>();
        conditions.put("isUser", true);
        Map<String, Integer> iterations = new HashMap<>();
        iterations.put("three", 3);
        Template template;
        try {
            template = new Template(bs.toString());
        } catch (UnexpectedConstruction C) {
            System.out.println("Unexpected Construction");
            return;
        }
        String temp;
        try {
            temp = template.fill(values, conditions, iterations);
        } catch (ArgumentNotFoundException E) {
            System.out.println("Argument Not Found");
            return;
        }
        System.out.println(temp);
    }
}
