package ru.nsu.fit.chuprakov;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(8080);
            while(true) {
                try (Socket clientSocket = serverSocket.accept();
                     InputStream inputStream = clientSocket.getInputStream();
                     OutputStream outputStream = clientSocket.getOutputStream();) {
                    Server server = new Server(inputStream, outputStream);
                    server.start();
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
