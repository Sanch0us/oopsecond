package ru.nsu.fit.chuprakov;

import ru.nsu.fit.chuprakov.httpHandler.HttpHandler;
import ru.nsu.fit.chuprakov.matcher.SegmentMatcher;
import ru.nsu.fit.chuprakov.serverErrorException.FileNotFoundException;

import java.util.LinkedList;
import java.util.List;

public class TreeMatcher {

    public SegmentMatcher matcher;

    private List<TreeMatcher> childs;

    private HttpHandler handler;

    private TreeMatcher(SegmentMatcher s){
        matcher = s;
        childs = new LinkedList<>();
        handler = null;
    }

    public TreeMatcher(){
        matcher = null;
        childs = new LinkedList<>();
        handler = null;
    }

    public void register(List<SegmentMatcher> matchers, HttpHandler h) {
        matchers.remove(0);
        if (matchers.isEmpty()) {
            handler = h;
            return;
        }
        for (TreeMatcher node : childs) {
            if (node.equals(matchers.get(0))) {
                node.register(matchers, h);
                return;
            }
        }
        TreeMatcher node = new TreeMatcher(matchers.get(0));
        childs.add(node);
        node.register(matchers, h);
    }

    public HttpHandler getHandler(HttpRequest request) throws FileNotFoundException {
        String segment = request.getCurrentSegment();
        if(segment == null){
            return handler;
        }
        for(TreeMatcher node: childs){
            if(node.matcher.isMatching(segment)){
                if(matcher.getName() != null){
                    request.addInfo(matcher.getName(), segment);
                }
                return node.getHandler(request);
            }
        }
        if(handler != null){
            return handler;
        }
        throw new FileNotFoundException();
    }
}