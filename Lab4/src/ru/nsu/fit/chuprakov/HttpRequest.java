package ru.nsu.fit.chuprakov;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HttpRequest {
    private InputStream body;

    private String path;

    private List<String> segments;

    private String[] headers;

    private String[] parameters;

    private String method;

    private Map<String, String> pathInfo;

    private int counter;

    public HttpRequest(InputStream body, String path, String[] headers, String[] parameters, String method) {
        this.body = body;
        this.path = path;
        segments = Stream.of(path.split("/")).collect(Collectors.toList());
        if(segments.size() != 0) {
            segments.remove(0);
        }
        this.headers = headers;
        this.parameters = parameters;
        this.method = method;
        counter = 0;
        pathInfo = new HashMap<>();
    };

    public void addInfo(String name, String segm){
        pathInfo.put(name, segm);
    }

    public String getCurrentSegment(){
        if(segments.size() <= counter){
            return null;
        }
        counter++;
        return segments.get(counter - 1);
    };

    public String getPath(){
        return path;
    }

    public String[] getHeaders(){
        return headers;
    };

    public InputStream getBody(){
        return body;
    };

    public String[] getParameters(){
        return parameters;
    }

    public String getMethod(){
        return method;
    }
}
