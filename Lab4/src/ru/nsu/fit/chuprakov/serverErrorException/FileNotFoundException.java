package ru.nsu.fit.chuprakov.serverErrorException;

public class FileNotFoundException extends ServerErrorException {

    private static final int code = 404;

    @Override
    public int getCode() {
        return code;
    }
}
