package ru.nsu.fit.chuprakov.serverErrorException;

public class InternalServerErrorException extends ServerErrorException {

    private static final int code = 500;

    @Override
    public int getCode() {
        return 500;
    }
}
