package ru.nsu.fit.chuprakov.serverErrorException;

public abstract class ServerErrorException extends Exception{
    public abstract int getCode();
}
