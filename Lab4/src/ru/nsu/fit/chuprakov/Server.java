package ru.nsu.fit.chuprakov;

import ru.nsu.fit.chuprakov.httpHandler.HttpHandler;
import ru.nsu.fit.chuprakov.httpHandler.PathHandler;
import ru.nsu.fit.chuprakov.matcher.SegmentMatcher;
import ru.nsu.fit.chuprakov.matcher.StringMatcher;
import ru.nsu.fit.chuprakov.serverErrorException.ServerErrorException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

public class Server {
    private InputStream inputStream;
    private OutputStream outputStream;

    Server(InputStream inputStream, OutputStream outputStream){
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    public void start() throws IOException {
        TreeMatcher head = new TreeMatcher();
        getTree(head);
        try{
            HttpParser parser = new HttpParser();
            HttpRequest request = parser.parse(inputStream);
            HttpHandler handler = head.getHandler(request);
            HttpResponse response = handler.handle(request);
            response.write(outputStream);
        } catch (ServerErrorException e) {
            HttpResponse response = new HttpResponse(e.getCode(), null);
            response.write(outputStream);
        }
    }

    private void getTree(TreeMatcher head){
        List<SegmentMatcher> list = new LinkedList<>();
        list.add(new StringMatcher("static", null));
        head.register(list, new PathHandler());
    }
}
