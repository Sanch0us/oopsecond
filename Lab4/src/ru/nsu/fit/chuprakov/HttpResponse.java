package ru.nsu.fit.chuprakov;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class HttpResponse {

    private int code;

    private FileInputStream fileStream;

    private static final String VERSION = "HTTP/1.1";

    public HttpResponse(int code, FileInputStream fileStream){
        this.code = code;
        this.fileStream = fileStream;
    }

    public void write(OutputStream out) throws IOException{
        out.write(makeResponse(code).getBytes());
        if(code != 200){
            return;
        }
        byte[] buffer = new byte[2048];
        int countOfByte;
        while ((countOfByte = fileStream.read(buffer)) != -1) {
            out.write(buffer, 0, countOfByte);
        }
    }

    private String makeResponse(int code) {
        return VERSION + getAnswer(code) + "\n\n";
    }

    private String getAnswer(int code) {
        switch (code) {
            case 200:
                return " 200 OK";
            case 404:
                return " 404 Not Found";
            case 405:
                return " 405 Method Not Allowed";
            default:
                return " 500 Internal Server Error";
        }
    }

}
