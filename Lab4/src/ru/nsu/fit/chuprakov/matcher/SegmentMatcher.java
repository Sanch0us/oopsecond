package ru.nsu.fit.chuprakov.matcher;

public interface SegmentMatcher {

    public boolean isMatching(String segment);

    public boolean equals(SegmentMatcher s);

    public String getName();
}
