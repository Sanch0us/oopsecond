package ru.nsu.fit.chuprakov.matcher;

public class StringMatcher implements SegmentMatcher {

    String segment;

    String name;

    public StringMatcher(String segment, String name){
        this.segment = segment;
        this.name = name;
    }

    @Override
    public boolean isMatching(String segment) {
        return this.segment.equals(segment);
    }

    @Override
    public boolean equals(SegmentMatcher s) {
        if(this.getClass().equals(s.getClass())){
            return s.isMatching(segment);
        }
        return false;
    }

    @Override
    public String getName() {
        return name;
    }
}
