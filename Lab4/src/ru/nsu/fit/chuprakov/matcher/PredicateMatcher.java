package ru.nsu.fit.chuprakov.matcher;

import java.util.function.Predicate;

public class PredicateMatcher implements SegmentMatcher{

    Predicate<String> predicate;

    String name;

    public PredicateMatcher(Predicate<String> predicate, String name){
        this.predicate = predicate;
        this.name = name;
    }

    @Override
    public boolean isMatching(String segment) {
        return predicate.test(segment);
    }

    @Override
    public boolean equals(SegmentMatcher s) {
        return false;
    }

    @Override
    public String getName() {
        return name;
    }
}
