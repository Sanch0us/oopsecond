package ru.nsu.fit.chuprakov.matcher;

public class AnyMatcher implements SegmentMatcher {

    String name;

    public AnyMatcher(String name){
        this.name = name;
    }

    @Override
    public boolean isMatching(String segment) {
        return true;
    }

    @Override
    public boolean equals(SegmentMatcher s) {
        return this.getClass().equals(s.getClass());
    }

    @Override
    public String getName() {
        return name;
    }
}
