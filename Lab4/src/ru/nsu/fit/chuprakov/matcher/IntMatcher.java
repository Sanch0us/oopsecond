package ru.nsu.fit.chuprakov.matcher;

public class IntMatcher implements SegmentMatcher {

    private String name;

    public IntMatcher(String name){
        this.name = name;
    }

    @Override
    public boolean isMatching(String segment) {
        try {
           int i = Integer.parseInt(segment);
           return true;
        }
        catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean equals(SegmentMatcher s) {
         return this.getClass().equals(s.getClass());
    }

    @Override
    public String getName() {
        return name;
    }
}
