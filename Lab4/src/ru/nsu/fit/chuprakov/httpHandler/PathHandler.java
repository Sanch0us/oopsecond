package ru.nsu.fit.chuprakov.httpHandler;

import ru.nsu.fit.chuprakov.HttpRequest;
import ru.nsu.fit.chuprakov.HttpResponse;
import ru.nsu.fit.chuprakov.serverErrorException.FileNotFoundException;
import ru.nsu.fit.chuprakov.serverErrorException.ServerErrorException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;

public class PathHandler implements HttpHandler {
    private static final String DEFAULT_CATALOG = "static";
    private static final int OK = 200;
    private static final int INTERNAL_ERROR = 500;

    @Override
    public HttpResponse handle(HttpRequest request) {
        try{
            FileInputStream fileStream = new FileInputStream(getFile(request.getPath()));
            return new HttpResponse(OK, fileStream);
        } catch (IOException e) {
            return new HttpResponse(INTERNAL_ERROR, null);
        } catch (ServerErrorException e) {
            return new HttpResponse(e.getCode(), null);
        }
    }


    private File getFile(String path) throws ru.nsu.fit.chuprakov.serverErrorException.FileNotFoundException {
        path = path.replace('/', File.separatorChar);
        path = DEFAULT_CATALOG + path;
        File f = Paths.get(path).toFile();
        if (f.isDirectory()) {
            path += File.separator + "index.html";
            f = Paths.get(path).toFile();
        }
        if (!f.exists()) {
            throw new FileNotFoundException();
        }
        return f;
    }

}
