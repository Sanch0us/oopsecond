package ru.nsu.fit.chuprakov.httpHandler;

import ru.nsu.fit.chuprakov.HttpRequest;
import ru.nsu.fit.chuprakov.HttpResponse;

public interface HttpHandler {
    HttpResponse handle(HttpRequest request);
}
