package ru.nsu.fit.chuprakov;

import java.util.LinkedList;

public class ThreadPool {

    private int numberOfThreads;
    private PoolWorker[] threads;
    private final LinkedList<Runnable> queue;

    public ThreadPool(int numberOfThreads){
        this.numberOfThreads = numberOfThreads;
        threads = new PoolWorker[numberOfThreads];
        queue = new LinkedList<>();
        for (Thread thread: threads){
            thread = new PoolWorker();
            thread.start();
        }
    }


    public void Submit(Runnable task){
        synchronized (queue) {
            queue.addFirst(task);
            queue.notify();
        }
    }

    private class PoolWorker extends Thread{
        @Override
        public void run() {
            Runnable r;
            synchronized (queue) {
                while (true) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    r = queue.removeLast();
                    r.run();
                }
            }
        }
    }
}
