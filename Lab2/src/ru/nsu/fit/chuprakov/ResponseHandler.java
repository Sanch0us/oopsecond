package ru.nsu.fit.chuprakov;

import ru.nsu.fit.chuprakov.serverErrorException.FileNotFoundException;
import ru.nsu.fit.chuprakov.serverErrorException.ServerErrorException;

import java.io.*;
import java.nio.file.Paths;

public class ResponseHandler {
    private static final String DEFAULT_CATALOG = "static";
    private static final String VERSION = "HTTP/1.1";

    public void write(InputStream in, OutputStream out) throws IOException {
        int code = 200;
        try (FileInputStream fileStream = new FileInputStream(getFile(new HttpParser().parse(in).getPath()))) {
            out.write(makeResponse(code).getBytes());
            byte[] buffer = new byte[2048];
            int countOfByte;
            while ((countOfByte = fileStream.read(buffer)) != -1) {
                out.write(buffer, 0, countOfByte);
            }
        } catch (IOException e) {
            out.write(makeResponse(500).getBytes());
        } catch (ServerErrorException e) {
            out.write(makeResponse(e.getCode()).getBytes());
        }
    }

    private String makeResponse(int code) {
        return VERSION + getAnswer(code) + "\n\n";
    }

    private File getFile(String path) throws FileNotFoundException {
        path = path.replace('/', File.separatorChar);
        path = DEFAULT_CATALOG + path;
        File f = Paths.get(path).toFile();
        if (f.isDirectory()) {
            path += File.separator + "index.html";
            f = Paths.get(path).toFile();
        }
        if (!f.exists()) {
            throw new FileNotFoundException();
        }
        return f;
    }

    private String getAnswer(int code) {
        switch (code) {
            case 200:
                return " 200 OK";
            case 404:
                return " 404 Not Found";
            case 405:
                return " 405 Method Not Allowed";
            default:
                return " 500 Internal Server Error";
        }
    }

}
