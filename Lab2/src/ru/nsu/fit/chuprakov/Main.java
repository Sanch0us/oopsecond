package ru.nsu.fit.chuprakov;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String[] args) {
        try{
            ServerSocket serverSocket = new ServerSocket(8080);
            while(true) {
                try(Socket clientSocket = serverSocket.accept()) {
                    try (InputStream inputStream = clientSocket.getInputStream()) {
                        try (OutputStream outputStream = clientSocket.getOutputStream()) {
                            ResponseHandler responseHandler = new ResponseHandler();
                            responseHandler.write(inputStream, outputStream);
                        }
                    }
                }
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
