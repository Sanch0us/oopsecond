package ru.nsu.fit.chuprakov.serverErrorException;

public class MethodNotAllowedException extends ServerErrorException{

    private static final int code = 405;

    @Override
    public int getCode() {
        return code;
    }
}
