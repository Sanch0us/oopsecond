package ru.nsu.fit.chuprakov;

import java.io.*;

public class RequestHandler {

    private InputStream body;

    private String path;

    private String[] headers;

    private String[] parameters;

    private String method;

    public RequestHandler(InputStream body, String path, String[] headers, String[] parameters, String method){
        this.body = body;
        this.path = path;
        this.headers = headers;
        this.parameters = parameters;
        this.method = method;
    };


    public String getPath(){
        return path;
    };

    public String[] getHeaders(){
        return headers;
    };

    public InputStream getBody(){
        return body;
    };

    public String[] getParameters(){
        return parameters;
    }

    public String getMethod(){
        return method;
    }
}
