package ru.nsu.fit.chuprakov;

import ru.nsu.fit.chuprakov.serverErrorException.*;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;


public class HttpParser {
    public RequestHandler parse(InputStream inputStream) throws IOException, ServerErrorException {
        BufferedInputStream stream = new BufferedInputStream(inputStream);
        byte [] buffer = new byte[2048];
        int i = 0;
        while(true){
            if(stream.read(buffer, i, 1) < 0){
                throw new InternalServerErrorException();
            }
            if(i < 3){
                i++;
                continue;
            }
            if((buffer[i] == '\n')&&(buffer[i - 1] == '\n') ||
                    (buffer[i] == '\n')&&(buffer[i - 1] == '\r')&&(buffer[i-2] == '\n')&&(buffer[i - 3] == '\r')){
                break;
            }
            i++;
        }
        String request = new String(buffer);
        return new RequestHandler(stream, getPath(request), getHeaders(request), getQueryParameters(request), getMethod(request));
    };

    private String getPath(String request) throws ServerErrorException {
        String [] splits = request.split("\n|\r\n");
        String [] firstString = splits[0].split(" ");
        if(!firstString[0].equals("GET")){
            throw new MethodNotAllowedException();
        }
        if((firstString[1].length() > 1)&&(firstString[1].charAt(firstString[1].length() - 1) == '/')){
            throw new FileNotFoundException();
        }
        return firstString[1];
    };

    private String[] getHeaders(String request){
        String [] splits = request.split("\n|\r\n");
        String [] headers = new String[splits.length - 1];
        System.arraycopy(splits, 1, headers, 0, headers.length);
        return headers;
    }

    private String[] getQueryParameters(String request){
        return request.split(" |(?)")[2].split("&");
    }

    private String getMethod(String request){
        return request.split(" ")[0];
    }
}
